// JavaScript Array

// Array basic structure
/*Syntax:
	let/const arrayName = [elementA, elementB, elementC, ....];
*/
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
// Possible use of mixed elements in an array but is not recommended
let mixedArray = [12, 'Asus', null, undefined, true];

let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];
console.log(grades);
console.log(computerBrands[2]);
console.log(computerBrands[7]);
console.log(computerBrands[2],computerBrands[7]);

// reassign array values
console.log('Array before reassignment');
console.log(myTasks);
myTasks[0] = 'Hello World';
console.log('Array after reassignment');
console.log(myTasks);

// Array Methods

// Mutator Methods = are functions that 'mutate' or change an array after they are created

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
// push() = adds an element in the end of an arry and returns the arrays length
/*Syntax:
	arrayName.push();
*/

console.log('Current array:');
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method:');
console.log(fruits);

// pop() = remove the last element in an array and returns the removed element
/*Syntax:
	arrayName.pop();
*/
let removedFruit = fruits.pop()
console.log(removedFruit);
console.log('Mutated array from pop method:');
console.log(fruits);

// unshift() = add one or more element at the beginning of an array
/*Syntax:
	arrayName.unshift('elementA');
	arrayName.unshift('elementA', elementB);
*/
fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method');
console.log(fruits);

// shift() = removes an element at the beginning of an array and returns the removed element
/*Syntax:
	arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method: ');
console.log(fruits);


// splice() = simultaneously removes elementss from a specified index number and adds elements
/*Syntax:
	arrayName.splice(startingIndex, deleteCount, elementToBeAdded);
*/
fruits.splice(1, 2, 'Lime' , 'Cherry', 'Pineapple');
console.log('Mutated array from splice method');
console.log(fruits);

// sort() = rearranges the array elements in alphanumeric order
/*Syntax:
	arrayName.sort();
*/

fruits.sort();
console.log('Mutated array from sort method:');
console.log(fruits);

// reverse() = reverse the order of array elements
/*Syntax:
	arrayName.reverse();
*/
fruits.reverse();
console.log('Mutated array from reverse method');
console.log(fruits);

// Non-mutator methods = these are functions that do not modify or change an array after they are created

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log(countries);
/*
indexOf 
	= returns an index number of the first matching element found in an array
	=if NO match was found, the result will be -1.
*/
/*Syntax:
	arrayName.indexOf(searchValue);
*/
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf() method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf() method: ' + invalidCountry);

// lastIndexOf =  returns the index number of the last matching element found in an array
/*Syntax:
	arrayName.lastIndexOf(searchValue);
	arrayName.lastIndexOf(searchValue, fromIndex);
*/
// getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf() method: ' + lastIndex); 

// getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log('Result of lastIndexOf() method: ' + lastIndexStart);


// slice() = portions / slices elements from an array and returns a new array
/*Syntax:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, endingIndex);
*/

// Slicing off elements from specified index to the last element
let slicedArrayA = countries.slice(2);
console.log('Result from slice method:');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method: ');
console.log(slicedArrayB);

// toString() = returns an array as a string separated by commas
/*Syntax:
	arrayName.toString();
*/ 

let stringArray = countries.toString();
console.log('Result from string method:');
console.log(stringArray);

// concat() = combines two arrays and returns the combined result
/*Syntax:
	arrayA.concat(arrayB);
	arrayA.concat(elementA);
*/

let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale css', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from concat method: ' + tasks);

// Combining multiple arrays
console.log('Result from concat method: ');
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// Combining array with elements
let combinedTasks = tasks.concat('smell express', 'throw react');
console.log('Result from concat method: ' + combinedTasks);

// join() = returns and array as a string separated by specified separator string
/*
*/

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join()); 
console.log(users.join(' - ')); 
console.log(users.join(' , ')); 

/*iteration methods - are loops designed to perform repetitive task on arrays
*/